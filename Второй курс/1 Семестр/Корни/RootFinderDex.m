%����� ������ ������� ���������
function [ROOT] = RootFinderDex(a,b,epsilon,n)
ROOT = 0; %������
InterLeng = abs(b-a); %������ ����������
OldInterLeng = abs(b-a); %������� ���������� �� ���������� ����
while (InterLeng > epsilon)
    newX = (b+a)/2; %���������� �������� ����������
    X1 = linspace(a,newX,n); % ������ ��������������
    X2 = linspace(newX,b,n); % ������ ��������������
    for i = 2:length(X1) %������������ ������� ��������������
        if Func(X1(i))*Func(X1(i-1)) < 10^-10
            b = newX; %����� ������� ��� ����������� ����� �����
        end
    end
    for i = 2:length(X2) %������������ ������� ��������������
        if Func(X2(i))*Func(X2(i-1)) < 10^-10
            a = newX; %����� ������� ��� ����������� ����� �����
        end
    end
    InterLeng = abs(b-a); %���������� ������ ������ ����������  
    if InterLeng == OldInterLeng %���� ������ �� ��������, �� ���������� ������������ �������� � ��������� �����������
        break
    else
        OldInterLeng = InterLeng;
    end
end
ROOT = (a+b)/2; %�������� �����


end

