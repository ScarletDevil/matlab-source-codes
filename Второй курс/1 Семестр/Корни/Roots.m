clc
clear all
format long %��������� ���������� ������ ����� ������� (���� ���������������� ��� ������� ������,�������� ����� ��������������� ������!)
disp('---����� ������---');
disp('���� 1 : ������������ ������� ��������� ��������');
disp('��������� �������� �������� �� �������� ������ ������');
disp('������������� ������������� ����� �����');
epsilon = input(':> ');
epsilon = 1/(10^epsilon); 
clc
disp('���� 2 : ������������ ���������� ����� ������� � ������ ���������');
a = input('������� ������ ������� ���������: ');
b = input('������� ������� ������� ���������: ');
n = input('������� ���������� ����� �������: ');
clc
YRaw = Func(linspace(a,b,n)); %������ �������
X = linspace(a,b,n); %����
SuspIntervals = []; %���������,������������� �� ������� ������
IntervalPosition = 0; %����������, ���������� ��������� ������ ����������
for i = 2:length(YRaw) %���������� ������� ����������
    if YRaw(i)*YRaw(i-1) < 10^-10 
        SuspIntervals(IntervalPosition+2) = X(i);
        SuspIntervals(IntervalPosition+1) = X(i-1);
        IntervalPosition = IntervalPosition + 2;
    end
end
disp('---����� � ������������� �� ����� �����������---');
for i = 2:2:length(SuspIntervals)
    disp(['����������: [' num2str(SuspIntervals(i-1)) ' , ' num2str(SuspIntervals(i)) ']']);
    disp(['�������� Y: [' num2str(Func(SuspIntervals(i-1))) ' , ' num2str(Func(SuspIntervals(i))) ']']);
end
disp('<---����� ������--->');

RootsDex = [];
RootsNewton = [];
RootsSek = [];
DexCount = 1;
NewtonCount = 1;
SekCount = 1;
for i = 2:2:length(SuspIntervals) %����� ������
    RootsDex(DexCount) = RootFinderDex(SuspIntervals(i-1),SuspIntervals(i),epsilon,n);
    DexCount = DexCount + 1;
    RootsNewton(NewtonCount) = RootFinderNewt(SuspIntervals(i-1),SuspIntervals(i));
    NewtonCount = NewtonCount + 1;    
    RootsSek(SekCount) = RootFinderSek(SuspIntervals(i-1),SuspIntervals(i));
    SekCount = SekCount +1 ;
end
delay = input('������� ����� �������,����� ����������...');
clc
disp('---����� � ��������� ������---');
disp('---���������---');
disp(RootsDex');
disp('---------------');
disp('-----������----');
disp(RootsNewton');
disp('-------------');
disp('---�������---');
disp(RootsSek');
disp('-------------');
disp('<---����� ������--->');
