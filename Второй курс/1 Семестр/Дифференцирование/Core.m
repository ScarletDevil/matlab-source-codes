%����������� ������� �����������������
function [RESG] = Core(X,Y,numP,type)
RESG = []; % �������� �����������
switch type
    case 1 %����������������� ������������� ����������
        Cofs = ControlC(X,Y);
        for g=1:length(X)
        RES = 0;
        for i = 0:(length(X)-numP-1)
            RES = RES + Cofs(i+numP+1)*factorial(i+numP)/factorial(i)*X(g)^i;
        end
        RESG(g) = RES;
        end
    case 2 %����������������� ���������� ��������
    RESG = LagrangeC(X,Y,numP);  
    case 3 %����������������� ���������� �������
        h = (X(2)-X(1));
        purif = 10^-8; %�����, ��� ������� ����� �������� ����������� �� 0
        H = h^(numP);
        N = length(X);
        % ���������� �����
        for i=N+1:-1:2
            X(i) = X(i-1); 
        end
        X(1) = X(2) - h;
        X(N+1) = X(N)+h;
        Y = func(X);
        deltY = YDIF(Y,10,purif); %�������� ��������, �������� 10 ������������ ������ ������� ��� ����������� ��������
       for i=1:N
        RESG(i) = (deltY(i,numP+1))/H; %����������� �������� �����������
       end
       
	
end 
end


