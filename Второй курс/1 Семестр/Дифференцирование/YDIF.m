%���������� �������� ���������
function [YDifs] = YDIF(Y,block,purif)
YDifs(:,1) = Y'; %��������� ������������� ������� �������� ���������
for i = 2:length(Y)
    if i >= block
        break
    end
    for g = 2:length(Y)-i+2
        YDifs(g-1,i) = YDifs(g,i-1) - YDifs(g-1,i-1); 
        if abs(YDifs(g-1,i)) < purif
            YDifs(g-1,i) = 0;
        end
        %���������� ������� �������� ��������� 
        %�������� ������,���������� � ������ ������� �������� �����������
        %������� ���������� ��� ������������ ���� �� ������� ��������
    end
end
end

