%������� ���������������� ���������� �� 26.11.19
% ��������������, ��� ������ ��� ��������� - �������� �������, ������ ���-
% �����������
% �������� div = 1 �������� ������ �����������
function [] = GraphPlotter(X1,Y1,X2,Y2,div)
Graph2 = {'.b';'-r';1};
YDIV = [];
if div == 1
    YD = func(X2);
    for i = 1:length(YD)
        YDIV(i) = abs(YD(i) - Y2(i));
    end
%    plot(X1,Y1,Graph2{1},X2,Y2,Graph2{2},X2,YDIV,'LineWidth',Graph2{3});
%    leg = legend('�������� �������','����������������� �������','�����������');
%    set(leg,'Location','northwest');
else
    plot(X1,Y1,Graph2{1},X2,Y2,Graph2{2},'LineWidth',Graph2{3});
    leg = legend('�������� �������','����������� �������');
    set(leg,'Location','northwest');
end

xlabel('X');
ylabel('Y');
grid on
title(['����������� � �������� �������']);
end

