function [IntegralSum] = IntegrateFunction(a,b,type,points)
IntegralSum = NaN; %��������
switch type{1}
    case 'N-C' %����� �������-������
    %����������� ������
    NC1 = [2];
    NC2 = [1 1];
    NC3 = [1/3 4/3 1/3];
    NC4 = [1/4 3/4 3/4 1/4];
    NC5 = [7/45 32/45 4/15 32/45 7/45];
    NC6 = [19/144 25/48 25/72 25/72 25/48 19/144];
    NC7 = [41/420 18/35 9/140 68/105 9/140 18/35 41/420];
    NC8 = [751/8640 3577/8640 49/320 2989/8640 2989/8640 49/420 3577/8640 751/8640];
    NC9 = [989/14175 5888/14175 -928/14175 10496/14175 -908/2835 10496/14175 -928/14175 5888/14175 989/14175];
    if points > 9 || points < 1
        return
    end
    IntegralSum = 0;
    Xk = ConstructNewXPositions(a,b,points);
    SumCof = (b-a)/2; %���������� ����� ������
    SelectedArguments = []; 
    switch points
        case 1
            SelectedArguments = NC1;
        case 2
            SelectedArguments = NC2;
        case 3 
            SelectedArguments = NC3;
        case 4
            SelectedArguments = NC4;
        case 5
            SelectedArguments = NC5;
        case 6
            SelectedArguments = NC6;
        case 7
            SelectedArguments = NC7;
        case 8
            SelectedArguments = NC8;
        case 9
            SelectedArguments = NC9;
    end
    for i = 1:points
        IntegralSum = IntegralSum + SelectedArguments(i)*FunctionToCalculate(Xk(i));
    end
    IntegralSum = IntegralSum*SumCof;
%--------------------------------------------    
    case 'M-K' %����� �����-�����
    if points <= 0
        return
    end
     IntegralSum = 0;
     Xk = ConstructNewXPositions(a,b,points);
     SumCof = (b-a)/points;  
    for i = 1:points
        IntegralSum = IntegralSum + FunctionToCalculate(Xk(i));
    end 
%--------------------------------------------
    case 'Rectangle-C'
        IntegralSum = 0;
        IntegralSum = FunctionToCalculate((b-a)/2)*(b-a);
%--------------------------------------------
    case 'Trapezoid'
    IntegralSum = 0;
    SumCof = (b-a)/2;   
    IntegralSum =  SumCof*(FunctionToCalculate(b) + FunctionToCalculate(a));
%--------------------------------------------
    case 'Simpson'
    IntegralSum = 0;
    SumCof = (b-a)/6;  
    IntegralSum = SumCof*(FunctionToCalculate(a) + 4*FunctionToCalculate((b+a)/2) + FunctionToCalculate(b));
    %--------------------------------------------
    case 'Lobatto'
        %����������� ������
        LB1 = [1.333333334];
        
        LB2 = [0.8333333336 0.8333333336];
        
        LB3 = [0.5444444441 0.7111111111 0.5444444441];
        
        LB4 = [0.3784749568 0.5548583768 0.5548583768 0.3784749568];
        
        LB5 = [0.2768260474 0.4317453811 0.4876190476 0.4317453811 0.2768260474];

        LB6 = [0.2107042270 0.3411226926 0.4124587950 0.4124587950 0.3411226926 0.2107042270];

        LB7 = [0.1654953606 0.2745387131 0.3464285107 0.371592744 0.3464285107 0.2745387131 0.1654953606];

        LB8 = [0.1333059923 0.2248893413 0.2920426861 0.3275397613 0.3275397613 0.2920426861 0.2248893413 0.1333059923];

        LB9 = [0.1096122699 0.1871698739 0.2480481120 0.2868791238 0.3002175954 0.2868791238 0.2480481120 0.1871698739 0.1096122699];
        if points > 9 || points < 1
            return
        end
    IntegralSum = 0;
    Xk = ConstructNewXPositions(a,b,points);
    SumCof = (b-a)/2;
    SelectedArguments = [];
    switch points
        case 1
            SelectedArguments = LB1;
        case 2
            SelectedArguments = LB2;
        case 3 
            SelectedArguments = LB3;
        case 4
            SelectedArguments = LB4;
        case 5
            SelectedArguments = LB5;
        case 6
            SelectedArguments = LB6;
        case 7
            SelectedArguments = LB7;
        case 8
            SelectedArguments = LB8;
        case 9
            SelectedArguments = LB9;
    end
    for i = 1:points
        IntegralSum = IntegralSum + SelectedArguments(i)*FunctionToCalculate(Xk(i));
    end
    IntegralSum = IntegralSum*SumCof;
    IntegralSum = IntegralSum + (FunctionToCalculate(a)+FunctionToCalculate(b))*(b-a)/((points+1)*(points+2));    
end
end

function [Xk] = ConstructNewXPositions(a,b,n)
    tk = [];
    h = 2/(n-1);
    for k=1:n
        tk(k) = -1+(k-1)*h;
    end
    Xk = (a+b)/2 + (b-a)/2*tk;
end
%points1 = [0];
%points2 = [-0,4472135956 0,4472135956];
%points3 = [-0,6546536709 0 0,6546536709];
%points4 = [-0,7650553238 -0,2852315163 0,2852315163 0,7650553238];
%points5 = [-0,8302238963 -0,4688487936 0 0,4688487936 0,8302238963];
%points6 = [-0,8717401487 -0,5917001814 -0,2092992172 0,2092992172 0,5917001814 0,8717401487];
%points7 = [-0.8997579958 -0.6771862797 -0.3631174632 0 0.3631174632 0.6771862797 0.8997579958];
%points8 = [-0.9195339082 -0.7387738651 -0.4779249499 -0.1652789577 0.1652789577 0.4779249499 0.7387738651 0.9195339082];
%points9 = [-0.9340014304 -0.7844834737 -0.5652353270 -0.2957581356 0 0.2957581356 0.5652353270 0.7844834737 0.9340014304];