%�������� ������������ ���������� ���������
%������� ������� �� 1+2k, k=1..maxiter, �� ������ ������� ���������
%��������� ������� ��������, ���������� ������������ � ������������ � ��������
function [] = PrecisionControl(ethalon,precisionBorder,a,b,RType,n,maxiter)
for i = 1:maxiter
    X = linspace(a,b,2*i+1);
    IntSum = 0;
    for g = 2:length(X)-1
        IntSum = IntSum + IntegrateFunction(X(g-1),X(g),RType,n);
    end
    if abs(IntSum-ethalon) <= precisionBorder
        disp('�������� �������!');
        return
    end
    
end
disp('�������� �� �������!');
