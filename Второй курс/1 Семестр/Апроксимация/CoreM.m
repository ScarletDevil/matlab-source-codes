% ����������� ������� ���������� ������������
function [Cof1,Cof2] = CoreM(X,Y,aType)
Cof1 = 0;
Cof2 = 0;
PHIMean = 0;
PHI2Mean = 0;
PHIMean2 = 0;
y = 0;
yPHI = 0;
%---���������� �������� � ������������ � ������� ���������
for i=1:length(X) %���������� ���� ��� ��������
    PHIMean = PHIMean + PHIMeanFunc(X(i),Y(i),aType);
    PHI2Mean = PHI2Mean + PHI2MeanFunc(X(i),Y(i),aType);
    y = y + yFunc(X(i),Y(i),aType);
    yPHI = yPHI + yPHIFunc(X(i),Y(i),aType);
end
%---���������� �������� �������� ��� ������ ���������
PHIMean = PHIMean/length(X);
PHI2Mean = PHI2Mean/length(X);
PHIMean2 = PHIMean^2;
y = y/length(X);
yPHI = yPHI/length(X);
%---���������� ������������
Cof1 = (PHI2Mean*y-yPHI*PHIMean)/(PHI2Mean-PHIMean2);
Cof2 = (yPHI-PHIMean*y)/(PHI2Mean-PHIMean2);
end

function [RES] = PHIMeanFunc(x,y,type)
    switch type
        case 9 
            RES = x;
        case 10 
            RES = x^2;
        case 11
            RES = 1/x;
        case 12
            RES = log(x);
        case 13
            RES = exp(-x);
        case 14
            RES = sin(x);
        case 15
            RES = sqrt(x);
    end
end

function [RES] = PHI2MeanFunc(x,y,type)
    switch type
        case 9
            RES = x^2;
        case 10
            RES = x^4;
        case 11
            RES = 1/x^2;
        case 12
            RES = log(x)^2;
        case 13 
            RES = exp(-2*x);
        case 14 
            RES = sin(x)^2;
        case 15
            RES = x;
            
    end
end

function [RES] = yFunc(x,y,type)
    switch type
        case 9
            RES = y;
        case 10
            RES = y;
        case 11
            RES = y;
        case 12
            RES = y;
        case 13
            RES = y;
        case 14
            RES = y;
        case 15
            RES = y;
    end
end

function [RES] = yPHIFunc(x,y,type)
    switch type
        case 9
            RES = x*y;
        case 10
            RES = x^2*y;
        case 11
            RES = y/x;
        case 12
            RES = y*log(x);
        case 13
            RES = y*exp(-x);
        case 14
            RES = y*sin(x);
        case 15
            RES = y*sqrt(x);
    end
    
end

