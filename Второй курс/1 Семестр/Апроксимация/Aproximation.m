clear all
clc
disp('������������');
%---���� ���������� ��������� ������������---
a = input('������� ������ ������� ���������: ');
b = input('������� ������� ������� ���������: ');
n = input('������� ���������� �����: ');
%---���������� X � Y
X = linspace(a,b,n);
Y = func(X);
%---���������� ������� ��������������� ������� ��� �������������
plot(X,Y,'.');
xlabel('X');
ylabel('Y');
grid on
title(['������������ �������']);
%---����� ���� ������������
AproxType = 0; %����������,�������� ����� ������
ListOffunct = {'1) Const','2) Const*x','3) Const*x^2','4) Const/x','5) Const*Ln(x)','6) Const*exp(x)','7) Const*sin(x)','8) Const*sqr(x)','9) Const1+Const2*x','10) Const1+Const2*x^2','11) Const1+Const2/x','12) Const1+Const2*Ln(x)','13) Const1+Const2*exp(-x)','14) Const1+Const2*sin(x)','15) Const1+Const2*sqr(x)'}'; %������ ��������� ������������
disp('�������� ��� ������������: ');
disp(ListOffunct);
AproxType = input(':> ');
Cof = [0,0]; %������ ������� ������������ 
switch (AproxType)
    case {1,2,3,4,5,6,7,8}
        Cof(1) = CoreS(X,Y,AproxType); %����������� ������������
    case {9,10,11,12,13,14,15}
        [a,b] = CoreM(X,Y,AproxType); %���������� ������������
        Cof(1) = a;
        Cof(2) = b;
end
Yaprox = []; %������ �������, ���������� ��� ����������� ������������
switch (AproxType) %���������� ������� � ������������ � ������� ������� ���� ������������
    case 1
        for i=1:length(X)
            Yaprox(i) = Cof(1);
        end
    case 2
        for i=1:length(X)
            Yaprox(i) = X(i)*Cof(1);
        end
    case 3
        for i=1:length(X)
            Yaprox(i) = X(i)^2*Cof(1);
        end
    case 4
        for i=1:length(X)
            Yaprox(i) = Cof(1)/X(i);
        end
    case 5
        for i=1:length(X)
            Yaprox(i) = Cof(1)*log(X(i));
        end
    case 6
        for i=1:length(X)
            Yaprox(i) = Cof(1)*exp(-X(i));
        end        
    case 7
        for i=1:length(X)
            Yaprox(i) = Cof(1)*sin(X(i));
        end       
    case 8
        for i=1:length(X)
            Yaprox(i) = Cof(1)*sqrt(X(i));
        end 
    case 9
        for i=1:length(X)
            Yaprox(i) = Cof(1)+Cof(2)*X(i);
        end
    case 10
        for i=1:length(X)
            Yaprox(i) = Cof(1)+Cof(2)*X(i)^2;
        end     
    case 11 
        for i=1:length(X)
            Yaprox(i) = Cof(1)+Cof(2)/X(i);
        end
    case 12
        for i=1:length(X)
            Yaprox(i) = Cof(1)+Cof(2)*log(X(i));
        end  
    case 13
        for i=1:length(X)
            Yaprox(i) = Cof(1)+Cof(2)*exp(-X(i));
        end
    case 14
        for i=1:length(X)
            Yaprox(i) = Cof(1)+Cof(2)*sin(X(i));
        end
    case 15
        for i=1:length(X)
            Yaprox(i) = Cof(1)+Cof(2)*sqrt(X(i));
        end  
end
clc
disp(['���������� ��� ������������ ������������: ' num2str(Cof(1)) ' : ' num2str(Cof(2))]);
GraphPlotter(X,Y,X,Yaprox,1);

