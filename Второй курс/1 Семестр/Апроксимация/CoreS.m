% ����������� ������� ����������� ������������
function [Cof] = CoreS(X,Y,aType)
Cof = 0;
PHI = 0; 
yPHI = 0; 
%---���������� �������� � ������������ � ������� ���������
for i=1:length(X) %���������� ���� ��� ��������
    PHI = PHI2k(X(i),aType) + PHI;
    yPHI = YPH(X(i),Y(i),aType) + yPHI;
end
%---���������� �������� �������� ��� ������ ���������
PHI = PHI/length(X);
yPHI = yPHI/length(X);
%---���������� ������������
Cof = yPHI/PHI;
end

function [RES] = PHI2k(x,type)
switch(type)
    case 1
        RES = 1;
    case 2
        RES = x^2;
    case 3
        RES = x^4;
    case 4 
        RES = 1/x^2;
    case 5
        RES = log(x)^2;
    case 6
        RES = exp(-2*x);
    case 7
        RES = sin(x)^2;
    case 8
        RES = x;
end
end

function [RES] = YPH(x,y,type)
switch(type)
    case 1
        RES = y;
    case 2
        RES = y*x;
    case 3
        RES = y*x^2;
    case 4
        RES = y/x;
    case 5 
        RES = log(x)*y;
    case 6
        RES = y*exp(-x);
    case 7 
        RES = y*sin(x);
    case 8
        RES = y*sqrt(x);
end
end