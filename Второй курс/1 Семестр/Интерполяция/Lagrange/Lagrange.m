%������������ �� ������ ��������
function [ReadyGraphData] = Lagrange(x,DataToProceed)
    %DataToProceed - ������ ������� 2*n, ������ ������ �������� �������� �, ������ �
    %� - ��������,��� �������� ����������� ��������
    ReadyGraphData = 0; %������������� ����������,�������� � ���� ��������� ������ ���������
    ArrayParams = size(DataToProceed); %ArrayParams - ������,���������� ��������� ���������  �������������� ������� DataToProceed
    ArLength = ArrayParams(2); %ArLength - ������ ������� 
    GraphData = 0; % ������������� ���������� ��� ���������� 
    %---�������� ������������ �������� (1.9)---
     for i = 1:ArLength
       LagrangePolynome = 1; %���������� ��� ���������� ���������� ��������
       for j = 1:ArLength
         if j ~= i
           LagrangePolynome = LagrangePolynome*(x-DataToProceed(1,j))/(DataToProceed(1,i)-DataToProceed(1,j)); %���������� ���������� ��������
         end
       end
       GraphData = GraphData+LagrangePolynome*DataToProceed(2,i); %���������� �������� � ������������
     end
   %---����� ������ ��������� ������������ �������� (1.9)---
    ReadyGraphData = GraphData; %������������ ���������� ������ ��������� 
end

