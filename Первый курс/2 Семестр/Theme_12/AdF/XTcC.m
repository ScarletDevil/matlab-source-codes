function [AxTik] = XTcC(DT,step)
AxTik = [];
ch = 0;
if (DT(1) > -10^-10)
    ch = 1;
end
tck = (DT(2) - DT(1)+ch)/step;
for i = 1:fix(tck)+(1-ch)
    AxTik(i) = step*(i-1)+DT(1);
end
if length(AxTik) > 25
    AxTik = [];
end


