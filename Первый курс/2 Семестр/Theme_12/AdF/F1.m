function [y] = F1(x)
y = 0.5*x.^3+0.5*x.^2-11*x-12+0.05*exp(-0.6*x+3);
end

