function varargout = main_f(varargin)
% MAIN_F MATLAB code for main_f.fig
%      MAIN_F, by itself, creates a new MAIN_F or raises the existing
%      singleton*.
%
%      H = MAIN_F returns the handle to a new MAIN_F or the handle to
%      the existing singleton*.
%
%      MAIN_F('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN_F.M with the given input arguments.
%
%      MAIN_F('Property','Value',...) creates a new MAIN_F or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_f_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_f_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main_f

% Last Modified by GUIDE v2.5 08-Feb-2017 22:21:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_f_OpeningFcn, ...
                   'gui_OutputFcn',  @main_f_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before main_f is made visible.
function main_f_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main_f (see VARARGIN)

% Choose default command line output for main_f
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes main_f wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = main_f_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: place code in OpeningFcn to populate axes1
global GraphIsCreated GLC
GLC = {[1 0 0],[1 0 0]};
GraphIsCreated = false;
addpath(genpath('AdF'));


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)

% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global GraphIsCreated h1 h2 GLC
GraphIsCreated = true;
clc
cla
a = str2num(handles.edit1.String);
b = str2num(handles.edit2.String);
dx = str2num(handles.edit3.String);
dy = str2num(handles.edit4.String);
XtA = [];
YtA = [];
X = [];
Y = [];
min_y = 0;
max_y = 0;
if isempty(a) || isempty(b) || isempty(dx) || isempty(dy) || dx < 10^-10 || dy < 10^-10 || b < a
    waitfor(msgbox('One or multiple arguments is invalid','Error'));   
    return
else
    X = linspace(a,b,500);
    Y1 = F1(X);
    Y2 = F2(X);
    min_y = min(min(Y1,Y2));
    max_y = max(max(Y1,Y2));
    XtA = XTcC([a,b],dx);
    YtA = XTcC([min_y,max_y],dy);
end
xlim([a,b]);
ylim([min_y max_y]);
if isempty(XtA) == true
    handles.edit3.String = '1';
else
handles.axes1.XTick = XtA;
end
if isempty(YtA) == true
    handles.edit4.String = '2';
else
handles.axes1.YTick = YtA;
end
h1 = plot(X,Y1,'.');
set(h1,'Color',GLC{1});
h2 = plot(X,Y2,'.');
set(h2,'Color',GLC{2});
xlabel('X');
ylabel('Y');
grid on
title('������ �������');
if (a < 10^-10 && b > 10^-10) && (min_y < 10^-10 && max_y > 10^-10)
    line([0 0],[min_y max_y],'Color',[0 0 0],'LineWidth',1,'LineStyle','-');
    line([a,b],[0 0],'Color',[0 0 0],'LineWidth',1,'LineStyle','-');
end
leg = legend('Function 1','Function 2');
set(leg,'Location','northwestoutside');



% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global GraphIsCreated h1 h2 GLC 
if GraphIsCreated == true
    sw = menu('���� ������ ������� ���������?','������ 1','������ 2');
    Grcolor = uisetcolor([0 0 0],'���� �������');
    switch sw
        case 1
            set(h1,'Color',Grcolor);
            GLC{1} = Grcolor;
        case 2
            set(h2,'Color',Grcolor);
            GLC{2} = Grcolor;
    end
end
