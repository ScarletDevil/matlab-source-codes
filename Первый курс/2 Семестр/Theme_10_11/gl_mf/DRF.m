function [] = DRF( X,Y,titles )
plot(X,Y,'.');
title(titles{1});
xlabel(titles{2});
ylabel(titles{3});
axis ([min(X) max(X) min(Y) max(Y)]);
grid on;

end

