function [] = DRF2(FD,XD,YD,XTC,YTC)
fg = figure('Name','������.� ������ 0584 ������� 11','NumberTitle','off','Position',[250 60 900 700]);
ax = axes('Position',[0.06 0.08 0.85 0.85],'FontSize',14);
grid on;
xlabel('X');
ylabel('Y');
plot(FD(:,1),FD(:,2),'.r',FD(:,1),FD(:,3),'.b');
xlim(XD);
ylim(YD);
if (XD(1) < 10^-10 && XD(2) > 10^-10) && (YD(1) < 10^-10 && YD(2) > 10^-10)
    line([0,0],YD,'Color',[0 0 0],'LineWidth',1);
    line(YD,[0,0],'Color',[0 0 0],'LineWidth',1);
    line();
end
if isempty(XTC) ~= true
    set(gca,'XTick',XTC);
end
if isempty(YTC) ~= true
    set(gca,'YTick',YTC);
end
title('������ ���� �������');
end

