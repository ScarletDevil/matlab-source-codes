function [RES] = FFA(a,b,n)
X = linspace(a,b,n);
RES(:,1) = X;
for i = 1:length(X)
    RES(i,2) = F1(X(i));
    RES(i,3) = 52.2*cos(X(i)) + 5.5*X(i);
end
disp(RES);
end

