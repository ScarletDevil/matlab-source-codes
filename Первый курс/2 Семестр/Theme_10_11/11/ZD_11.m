clc
clear all

addpath(genpath('../gl_mf'));

a = input('������� ������ ������� ����������: ');
b = input('������� ������� ������� ����������: ');
n = input('������� ���������� ����� : ');

FD = FFA(a,b,n);

XD(1) = min(FD(:,1));
XD(2) = max(FD(:,1));

Y1min = min(FD(:,2));
Y1max = max(FD(:,2));

Y2min = min(FD(:,3));
Y2max = max(FD(:,3));
YD = [];

if Y1min < Y2min 
    YD(1) = Y1min;
else
    YD(1) = Y2min;
end

if Y1max > Y2max 
    YD(2) = Y1max;
else
    YD(2) = Y2max;
end

nsx = linspace(XD(1),XD(2),10);
tkX = input(['������� �������� ���������� ����� ����������� ������� �� ��� �,���� ��������������� ���������� ' num2str(nsx(2)-nsx(1)) ' : ']);

XTC = XTcC(XD,tkX);

nsY = linspace(YD(1),YD(2),10);
tkY = input(['������� �������� ���������� ����� ����������� ������� �� ��� Y,���� ��������������� ���������� ' num2str(nsY(2)-nsY(1)) ' : ']);

YTC = XTcC(YD,tkY);

clear nsx nsY a b n Y2min Y2max Y1min Y1max tkX tkY

DRF2(FD,XD,YD,XTC,YTC);
