clc 
clear all
XO = [];
n = input('Input number of elements: ');

for i = 1:n
    XO(i) = input(['Input element number: ' num2str(i) ': ']);
end
X = XO;
count = 0;
for i = 1:n+1
    for g = 1:n-1
        if (X(g) < X(g+1))
            prom = X(g);
            X(g) = X(g+1);
            X(g+1) = prom;
            prom = 0;
            count = count + 1;
        end
    end
end;
disp(['Moves: ' num2str(count)])
disp([XO',X']);