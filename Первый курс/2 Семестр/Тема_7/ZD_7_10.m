clc 
clear all
XO = [];
n = input('Input number of elements: ');

for i = 1:n
    XO(i) = input(['Input element number: ' num2str(i) ': ']);
end
X = XO;
for i = 1:n+1
    for g = 1:n-1
        if (X(g) > X(g+1))
            prom = X(g+1);
            X(g+1) = X(g);
            X(g) = prom;
            prom = 0;
        end
    end
end;
disp([XO',X']);