clc 
clear all
n = input('Input number of elements: ');
a = input('Input bottom border: ');
b = input('Input top border: ');
count = 0;
XO = randi([a,b],1,n);
X = XO;
for i = 1:n+1
    for g = 1:n-1
        if (X(g) > X(g+1))
            prom = X(g+1);
            X(g+1) = X(g);
            X(g) = prom;
            prom = 0;
            count = count + 1;
        end
    end
end;
disp(['Moves: ' num2str(count)])
disp([XO',X']);