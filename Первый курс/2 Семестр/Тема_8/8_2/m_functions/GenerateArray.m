function [ A ] = GenerateArray( x1,x2,n ) 
V = linspace(x1,x2,n);
a = [];
A = [];
for i = 1:n 
    a(i) = F1(V(i));
end
A(:,1) = a;
A(:,2) = V;
end
