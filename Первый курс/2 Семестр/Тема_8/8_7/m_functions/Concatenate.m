function [] = Concatenate(A,B,flag)
RES = [];
if (flag == 1 && length(A) == length(B))
    for i = 1:length(A)
        RES(i) = A(i) + B(i);
    end
    ShowNumeratedArray(RES);

else
    if (length(A) == length(B))
        RES = A + B;
        ShowNumeratedArray(RES);
    end
end
if (length(RES)<1)
    disp('Сложение невозможно!')

end

