function [ OUTP ] = F1( INP )
OUTP = [];
for i = 1:length(INP)
    omega = (4*INP(i)-INP(i)^3)/(INP(i)^(2)+6.6);
    OUTP(i) = 3*omega*cos(3*omega-0.3*omega);
end

end

