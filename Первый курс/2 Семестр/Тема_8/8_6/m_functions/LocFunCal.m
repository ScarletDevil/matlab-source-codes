function [ X ] = LocFunCal(A)
X = [];
for i = 1:length(A)
    X(i) = (2.2-0.3*A(i))*cos(A(i))-20.5*A(i);
end

end

