function [ Y ] = F1(X,x)
for i = 1:length(X)
    sums = 0;
    for g = 1:x
        p1 = (8.2*g-X(i));
        sums = sums + ((20-g)/g)*cos(p1);
    end
    Y(i) = sums;
end


end

