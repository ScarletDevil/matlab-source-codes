function [ S ] = GenerateNewArrayWithLinCof(MinMax_Arr,X,radius)
min_el = min(MinMax_Arr);
max_el = max(MinMax_Arr);
L = radius - (2*radius*max_el)/(max_el-min_el);
K = (2*radius)/(max_el-min_el);
S = X*K+L;


end

