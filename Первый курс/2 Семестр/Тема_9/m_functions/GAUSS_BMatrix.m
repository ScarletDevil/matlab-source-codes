function [FRC] = GAUSS_BMatrix()
global matrix1 matrix2 resultation
op_1 = input('�������� ������� ��� ���������� ������������: [1 - ������� 1] [2 - ������� 2] [3 - ���������] : ');
if op_1 ~= 1 && op_1 ~= 2 && op_1 ~= 3 && op_1 ~= 0
    disp('������� �������� �������');
    r = input('...');
    clear r
    return
end
S1 = [];
switch op_1
    case 1
        S1 = matrix1;
    case 2
        S1 = matrix2;
    case 3
        S1 = resultation;
    case 0
        return
end
S1O = S1;
sizes = size(S1);
if (sizes(1) ~= sizes(2)) || abs(det(S1)) < 10^-10
    disp('��� ������ ������� �������� �� ����������');
    r = input('...');
    return
end
FRC = eye(sizes(1),sizes(2));
for i = 1:sizes(2)
kof = S1(i,i);
S1(i,:) = S1(i,:)/kof;
FRC(i,:) = FRC(i,:)/kof;
for g = i+1:sizes(1)
    KF = S1(g,i);
    S1(g,1:sizes(2)) = S1(g,1:sizes(2)) - KF*S1(i,1:sizes(2));
    FRC(g,1:sizes(2)) = FRC(g,1:sizes(2)) - KF*FRC(i,1:sizes(2));
end
end
for i = sizes(2):-1:1
    for g = i-1:-1:1
        KF = S1(g,i);
        S1(g,:) = S1(g,:) - S1(i,:)*KF;
        FRC(g,:) = FRC(g,:) - FRC(i,:)*KF;
    end
end
disp('�������� �������: ');
disp(FRC);
disp('�������� ������������ ���������� (A*A-1 = E ?): ');
disp(S1O*FRC);
choise = (input('������� "1",���� ������ ������ ��������� � ������,��� ����� ������ �����/����� ��� ������: '));
switch(choise)
    case 1
         resultation = FRC;
    otherwise
        return
end
