function [] = BMatrix()
global matrix1 matrix2 resultation ShowRes
op_1 = input('�������� ������� ��� ���������� �������� �������: [1 - ������� 1] [2 - ������� 2] [3 - ���������] : ');
if op_1 ~= 1 && op_1 ~= 2 && op_1 ~= 3 && op_1 ~= 0
    disp('������� �������� �������');
    r = input('...');
    clear r
    return
end
S1 = [];
switch op_1
    case 1
        S1 = matrix1;
    case 2
        S1 = matrix2;
    case 3
        S1 = resultation;
    case 0 
        return
end
Determ = Det_B(S1);
if isnan(Determ) == true || abs(Determ) < 10^-10
    disp('�������� ������� ������������');
    r = input('...');
    clear r
    return
end
Adm = AdditionalDeterm(S1);
Adm = MatrixTranspose_B(Adm);
resultation = Adm/Determ;
ShowRes = true;
end

