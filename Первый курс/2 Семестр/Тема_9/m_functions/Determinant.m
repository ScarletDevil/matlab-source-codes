function [] = Determinant()
global matrix1 matrix2 resultation ShowRes
op_1 = input('�������� ������� ��� ���������� ������������: [1 - ������� 1] [2 - ������� 2] [3 - ���������] : ');
if op_1 ~= 1 && op_1 ~= 2 && op_1 ~= 3 && op_1 ~= 0
    disp('������� �������� �������');
    r = input('...');
    clear r
    return
end
S1 = [];
switch op_1
    case 1
        S1 = matrix1;
    case 2
        S1 = matrix2;
    case 3
        S1 = resultation;
    case 0
        return
end
check_1 = size(S1);
if (check_1(1) ~= check_1(2))
    disp('������������ ���������� ��� ������ �������!');
    r = input('...');
    clear r
    return
end
S1 = GaussTriangle(S1,check_1(1));
determ = 1;
for i = 1:check_1(1)
    determ = determ*S1(i,i);
end
resultation = determ;
ShowRes = true;
end

