function [Xs] = ExtractX_2(RwM,m,n)

for i = m:-1:1
    ARG = [];
    k = 1;
    for g = i+1:n-1
        ARG(k) = RwM((n-g+i),n)*RwM(i,g);
        %RwM(i,g) = 0;
        k = k+1;
    end
    k = 1;
    ARG = ARG*(-1);
    ARGN = sum(ARG);
    RwM(i,n) = RwM(i,n)+ARGN;
    clear ARG ARGN;
    if abs(RwM(i,i)) < 10^-10
        RwM(i,n) = 0;
    else
        RwM(i,n) = RwM(i,n) / RwM(i,i);
        RwM(i,i) = 1;
    end
    Xs{m-i+1} = num2str(RwM(i,n));
end
disp(RwM);
end

