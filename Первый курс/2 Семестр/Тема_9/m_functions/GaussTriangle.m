function [RES] = GaussTriangle(MATRIX_0,n)
for i = 1:n-1
    K = [];
    for g = i+1:n
        K(g) = MATRIX_0(g,i)/MATRIX_0(i,i)*(-1);
    end
    
    for g = i+1:length(K)
        MATRIX_0(g,:) = MATRIX_0(g,:) + MATRIX_0(i,:)*K(g);
    end
end
RES = MATRIX_0;
end

