function [determ] = Det_B(Matrix_0)
check_1 = size(Matrix_0);
if (check_1(1) ~= check_1(2))
    return 
end
S1 = GaussTriangle(Matrix_0,check_1(1));
determ = 1;
for i = 1:check_1(1)
    determ = determ*S1(i,i);
end
if determ == 0
    return 
end


