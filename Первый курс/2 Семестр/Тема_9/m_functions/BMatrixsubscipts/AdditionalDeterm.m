function [DetMatrix] = AdditionalDeterm(Matrix_0)
size_1 = size(Matrix_0);
DetMatrix = [];
for i = 1:size_1(1)
    for g = 1:size_1(2)
        PrDet = Matrix_0;
        PrDet(i,:) = [];
        PrDet(:,g) = [];
        PrDet = Det_B(PrDet);
        DetMatrix(i,g) = PrDet*(-1)^(i+g);
    end
end
    

end


