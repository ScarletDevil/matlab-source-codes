function [S1] = MatrixTranspose_B(Matrix_0)
S1 = Matrix_0;
check_1 = size(S1);
for i = 2:check_1(1)
    for g = 1:i-1
        a = S1(i,g);
        S1(i,g) = S1(g,i);
        S1(g,i) = a;
    end
end
end

